import NapDemoApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.*;
import java.io.*;
import java.util.concurrent.*;

class PeerServerFileSharingImpl extends PeerServerFileSharingPOA
{
    private ORB orb;
    private String folderPath;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    public void setFolderPath(String folderPath_val) {
        folderPath = folderPath_val;
    }

    public String getFolderPath() {
        return folderPath;
    }

    /**
     * Obtain downloads filename invoked by the client peer to the
     * peer server.
     *
     * @param String filename [The file requested by the client peer.]
     * @return [MyFileObj]
     */
    public MyFileObj obtain(String filename, String folderPath) {
        File file = new File(folderPath + filename);

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            String contents = "";
            while((line = br.readLine()) != null) {
                contents += line;
                contents += "\n";
            }

            System.out.println("Transferring file `" + filename + "` to Interactive Peer...");
            return new MyFileObj(file.getName(), contents, file.getPath(), (int)file.length());
        }
        catch(Exception e) {
            System.err.println("ERROR: " + e);
            e.printStackTrace(System.out);
        }

        // There was a problem retrieving the file...
        return null;
    }

    /**
     * Shutdown peer server from client peer.
     *
     * @return [void]
     */
    public void shutdown() {
        orb.shutdown(false);
    }
}

public class IndexPeer
{
    static IndexServerFileSharing indexServerFileSharingImpl;

    public static final String INDEX_SERVER_NAME = "IndexServer";
    public static final String PEER_SERVER_NAME = "PeerServer";

    static final int PEER_SERVER_THREAD_POOL_SIZE = 5;

    // Create thread pool to handle multiple requests concurrently
    static ExecutorService peerThreadPool = Executors.newFixedThreadPool(PEER_SERVER_THREAD_POOL_SIZE);

    /**
     * Sets a random file size for file within the peerFolderPath.
     * File sizes range from 1 to 15 KB.
     *
     * @param String peerFolderPath [Path to the folder that holds the files for the peer
     *                                within the project directory.]
     * @param File file [File that will be given a random file size.]
     */
    public static void setRandomFileSize(String peerFolderPath, File file) {
        try {
            File f = new File(peerFolderPath + file.getName());
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            // Set file random size between 1 and 15 KB
            Random rn = new Random();
            int randFileSize = rn.nextInt(15 - 1 + 1) + 1;

            raf.setLength(1024 * randFileSize);
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    /**
     * Client registers with the indexing server using peerID and its files in peerFolderPath.
     *
     * @param  int peerID [ID of the peer. Either '1' or '2' or '3' which represents
     *                     the three peers]
     * @param  String peerFolderPath [Path to the folder that holds the files for the peer
     *                                within the project directory.]
     * @return [boolean]
     */
    public static boolean registerWithIndexServer(int peerID, String peerFolderPath) {
        File peerFolder = new File(peerFolderPath);
        File[] peerFiles = peerFolder.listFiles();

        List<MyFileObj> files = new ArrayList<MyFileObj>();

        for(File f: peerFiles) {
            if(f.isFile()) {
                setRandomFileSize(peerFolderPath, f);

                try(BufferedReader br = new BufferedReader(new FileReader(peerFolderPath + f.getName()))) {
                    String line = null;
                    String contents = "";
                    while((line = br.readLine()) != null) {
                        contents += line;
                        contents += "\n";
                    }

                    MyFileObj myfile = new MyFileObj(f.getName(), contents, peerFolderPath, (int)f.length());
                    // System.out.println(myfile.fileName);
                    // System.out.println(myfile.fileData);
                    files.add(myfile);
                }
                catch(Exception e) {
                    System.err.println("ERROR: " + e);
                    e.printStackTrace(System.out);
                }
            }
        }

        Peer peer = new Peer(peerID, files.toArray(new MyFileObj[0]));

        // Register peer with index server
        return indexServerFileSharingImpl.registry(peer.peerID, peer.files);
    }

    /**
     * Initialize the peer server so that it can receive invocations from other client
     * peers.
     *
     * @param String args[]
     * @return [void]
     */
    public static void initPeerServer(String args[]) {
        // Thread pool will handle multiple requests from other peer clients
        peerThreadPool.submit(() -> {
            try {
                // create and initialize the peer server ORB
                ORB peerServerORB = ORB.init(args, null);

                // get reference to rootpoa & activate the POAManager
                POA rootpoa = POAHelper.narrow(peerServerORB.resolve_initial_references("RootPOA"));
                rootpoa.the_POAManager().activate();

                // create servant and register it with the peer server ORB
                PeerServerFileSharingImpl peerServerImpl = new PeerServerFileSharingImpl();
                peerServerImpl.setORB(peerServerORB);

                // get object reference from the servant
                org.omg.CORBA.Object ref = rootpoa.servant_to_reference(peerServerImpl);
                PeerServerFileSharing href = PeerServerFileSharingHelper.narrow(ref);

                // get the root naming context
                // NameService invokes the name service
                org.omg.CORBA.Object objRef =
                    peerServerORB.resolve_initial_references("NameService");
                // Use NamingContextExt which is part of the Interoperable
                // Naming Service (INS) specification.
                NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                // bind the Object Reference in Naming
                NameComponent path[] = ncRef.to_name(PEER_SERVER_NAME + args[0]);
                ncRef.rebind(path, href);

                System.out.println("Peer server " + args[0] + " ready and waiting...\n");

                // wait for invocations from other peer clients
                peerServerORB.run();
            }
            catch(Exception e) {
                System.err.println("ERROR: " + e);
                e.printStackTrace(System.out);
            }
        });
    }

    /**
     * Main method of the index peer. Registers files with index server, starts the peer
     * server and waits for input about which file to download.
     *
     * @param String args[] [Only one argument is passed to start the peer, which is the peer
     *                       id: 1 or 2 or 3]
     * @return [void]
     */
    public static void main(String args[]) {
        if(args.length == 0) {
            System.out.print("ERROR -> usage: java IndexPeer 'peer ID'\n");
            System.out.println("Where 'peer ID' is 1 or 2 or 3\nPlease try again.");
            System.exit(1);
        }

        // Get a handle on the index server to register files
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            indexServerFileSharingImpl = IndexServerFileSharingHelper
                                            .narrow(ncRef.resolve_str(INDEX_SERVER_NAME));

            System.out.println("Obtained a handle on index server object: \n\n" + indexServerFileSharingImpl + "\n");

            // Register peer 1 files with index server
            int peerID = Integer.parseInt(args[0]);
            if(peerID == 1) {
                if(registerWithIndexServer(peerID, "peer-files/peer1/")) {
                    System.out.println("Peer client " + peerID + " successfully registered its files with the index server.\n");
                } else {
                    System.out.println("ERROR: There was a problem registering files for peer client " + peerID + ". Client is shutting down.\n");
                    System.exit(1);
                }
            }

            // Register peer 2 files with index server
            if(peerID == 2) {
                if(registerWithIndexServer(peerID, "peer-files/peer2/")) {
                    System.out.println("Peer client " + peerID + " successfully registered its files with the index server.\n");
                } else {
                    System.out.println("ERROR: There was a problem registering files for peer client " + peerID + ". Client is shutting down.\n");
                    System.exit(1);
                }
            }

            // Register peer 3 files with index server
            if(peerID == 3) {
                if(registerWithIndexServer(peerID, "peer-files/peer3/")) {
                    System.out.println("Peer client " + peerID + " successfully registered its files with the index server.\n");
                } else {
                    System.out.println("ERROR: There was a problem registering files for peer client " + peerID + ". Client is shutting down.\n");
                    System.exit(1);
                }
            }
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }

        // Initialize the peer as a server
        initPeerServer(args);
    }
}
