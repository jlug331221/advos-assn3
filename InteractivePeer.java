import NapDemoApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.*;
import java.io.*;

public class InteractivePeer
{
    static IndexServerFileSharing indexServerFileSharingImpl;
    static PeerServerFileSharing peerServerFileSharingImpl;

    public static final String INDEX_SERVER_NAME = "IndexServer";
    public static final String PEER_SERVER_NAME = "PeerServer";

    static final String INTERACTIVE_PEER_FOLDER_PATH = "peer-files/interactive-peer/";

    static Scanner kbd1 = new Scanner(System.in);
    static Scanner kbd2 = new Scanner(System.in);

    /**
     * Add file to interactive peer directory.
     *
     * @param MyFileObj myFileObj [File to add to the interactive peer directory.]
     * @return [void]
     */
    public static void addFileToInteractivePeerFolder(MyFileObj myFileObj) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        File f = new File(INTERACTIVE_PEER_FOLDER_PATH, myFileObj.fileName);

        try {
            if (! f.exists()) {
                f.createNewFile();

                // Write contents from downloaded file to new file in the interactive peer folder
                fw = new FileWriter(f);
    	        bw = new BufferedWriter(fw);
    	        bw.write(myFileObj.fileData);

                // Set the correct file size
                RandomAccessFile raf = new RandomAccessFile(f, "rw");
                raf.setLength(myFileObj.fileSize);

                System.out.println("\nFile `" + myFileObj.fileName + "` successfully downloaded.");
            }
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
        finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
			}
            catch (IOException ex) {
				ex.printStackTrace();
			}
        }
    }

    /**
     * Ensures that the user selects a peer from the list of peers provided.
     *
     * @param Peer[] peersThatHaveFile [List of peers that have the file available for download.]
     * @param int peerIDSelection [Peer ID selection taken from the user.]
     * @return [boolean]
     */
    public static boolean verifyPeerSelection(Peer[] peersThatHaveFile, int peerIDSelection) {
        for(Peer p: peersThatHaveFile) {
            if(p.peerID == peerIDSelection) {
                return true;
            }
        }

        return false;
    }

    /**
     * Select a peer from the list of peers to download the desired file.
     *
     * @param Peer[] peersThatHaveFile [List of peers that have the file available for download.]
     * @param String fileToDownload [File that will be downloaded from peer server.]
     * @param String[] args [Needed for the initialization of the ORB]
     * @return [MyFileObj]
     */
    public static MyFileObj selectPeerAndDownloadFile(Peer[] peersThatHaveFile, String fileToDownload, String[] args) {
        int peerIDSelection;
        String folderPath = null;

        System.out.print("\nSelect the peer to download `" + fileToDownload + "` and do so by entering the peer id: ");
        peerIDSelection = kbd2.nextInt();
        while(! verifyPeerSelection(peersThatHaveFile, peerIDSelection) || Integer.toString(peerIDSelection).equals("")) {
            System.out.println("\nYou didn't select a peer from the list of peers that have your desired download available. Please try again.");
            System.out.print("\nSelect the peer to obtain your desired file by the peer id: ");
            peerIDSelection = kbd2.nextInt();
        }

        folderPath = "peer-files/peer" + Integer.toString(peerIDSelection) + "/";

        // Get a handle on the peer server
        try {
            // create and initialize the ORB
            ORB download_orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                download_orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            peerServerFileSharingImpl = PeerServerFileSharingHelper
                                        .narrow(ncRef.resolve_str(PEER_SERVER_NAME + peerIDSelection));

            System.out.println("Obtained a handle on the peer server object: \n\n" + peerServerFileSharingImpl + "\n");
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }

        System.out.println("\nDownloading file...\n");
        return peerServerFileSharingImpl.obtain(fileToDownload, folderPath);
    }

    /**
     * Get user input as a file name to search. Use the file name to search for all clients
     * that have said file name and download it to client peer directory.
     *
     * @param String[] args [Needed for the initialization of the ORB]
     * @return [void]
     */
    public static void getUserInputForFiletoDownload(String[] args) {
        String fileToSearch = "";
        String correctFileNameAnswer = "";

        System.out.println("Note: The only file extensions available for searching are '.exe' and '.dat'");
        System.out.println("Press 'Q' to quit.");
        System.out.print("Search for a file: ");
        fileToSearch = kbd1.nextLine();

        while(! fileToSearch.equalsIgnoreCase("Q")) {
            System.out.print("\nIt looks like you want to search for `" + fileToSearch + "`. Is that correct? 'Y' or 'N': ");
            correctFileNameAnswer = kbd1.nextLine();

            if(correctFileNameAnswer.equalsIgnoreCase("Y")) {
                // Perform search
                Peer[] peersThatHaveFile = indexServerFileSharingImpl.search(fileToSearch);

                if(peersThatHaveFile.length > 0) {
                    // List peers that have file available for download
                    System.out.println("\nHere are the list of peers that have `" + fileToSearch + "` available for download:");
                    System.out.println("\n****************************************");
                    for(Peer p: peersThatHaveFile) {
                        System.out.println("* Peer with id: " + p.peerID);
                    }
                    System.out.println("****************************************");

                    // Download file
                    MyFileObj desiredFile = selectPeerAndDownloadFile(peersThatHaveFile, fileToSearch, args);

                    if(desiredFile != null) {
                        System.out.println("Desired filename: " + desiredFile.fileName);
                        System.out.println("Desired file size: " + desiredFile.fileSize + " KB");
                        System.out.println("Desired file origin folder path: " + desiredFile.folderPath);
                        System.out.println("Desired file content: " + desiredFile.fileData);
                    }

                    // Add File to interactive-peer folder
                    addFileToInteractivePeerFolder(desiredFile);
                } else {
                    System.out.println("\nIt looks like that file isn't available for download...");
                }
            }

            System.out.println("\nPress 'Q' to quit.");
            System.out.print("Search for a file: ");
            fileToSearch = kbd1.nextLine();
        }

        System.out.println("\nThank you for using the Napster Demo client! Come back soon for all of your illegal downloads.\n");
    }

    /**
     * Interactive peer main method.
     *
     * @param String args[]
     * @return [void]
     */
    public static void main(String args[]) {
        // Get a handle on the index server
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            indexServerFileSharingImpl = IndexServerFileSharingHelper
                                            .narrow(ncRef.resolve_str(INDEX_SERVER_NAME));

            System.out.println("Obtained a handle on index server object: \n\n" + indexServerFileSharingImpl + "\n");
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }

        // Prompt user to search for a file to download
        getUserInputForFiletoDownload(args);
    }
}
