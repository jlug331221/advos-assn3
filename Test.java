import NapDemoApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.*;
import java.io.*;

public class Test {
    static IndexServerFileSharing indexServerFileSharingImpl;
    static PeerServerFileSharing peerServerFileSharingImpl;

    public static final String INDEX_SERVER_NAME = "IndexServer";
    public static final String PEER_SERVER_NAME = "PeerServer";

    public static int calcAvgResponseTime(long[] requestTimes) {
        int total = 0;
        for(int i = 0; i < requestTimes.length; i++) {
            total += requestTimes[i];
        }

        return total / requestTimes.length;
    }

    /**
     * Make 1000 sequential requests to the server and output the average response time
     * for each request.
     *
     * @param String args[]
     * @return [void]
     */
    public static void concurrentRequestTest(String[] args) {
        // Get a handle on the index server
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args, null);

            // get the root naming context
            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            // Use NamingContextExt instead of NamingContext. This is
            // part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // resolve the Object Reference in Naming
            indexServerFileSharingImpl = IndexServerFileSharingHelper
                                            .narrow(ncRef.resolve_str(INDEX_SERVER_NAME));

            System.out.println("Obtained a handle on index server object: \n\n" + indexServerFileSharingImpl + "\n");

            long[] requestTimes = new long[1000];
            long startTime, endTime;
            int requestNum;
            for(int i = 0; i < 1000; i++) {
                startTime = System.currentTimeMillis();
                // Make a requests to the server
                indexServerFileSharingImpl.search("file10.dat");
                endTime = System.currentTimeMillis();
                requestTimes[i] = endTime - startTime;
                requestNum = i + 1;
                System.out.println("Time of request " + requestNum + ": " + requestTimes[i]);
            }
            System.out.println("The avgerage response is " + calcAvgResponseTime(requestTimes) + "ms");
        }
        catch(Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    /**
     * Interactive peer main method.
     *
     * @param String args[]
     * @return [void]
     */
    public static void main(String args[]) {
        concurrentRequestTest(args);
    }
}
