import NapDemoApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.*;
import java.util.concurrent.*;

class IndexServerFileSharingImpl extends IndexServerFileSharingPOA
{
    private ORB orb;
    private List<Peer> indexTable = new ArrayList<Peer>();

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    /**
    * Prints out the contents of the index table.
    *
    * @return [void]
    */
    public void printContentsOfIndexTable() {
        for(Peer p: indexTable) {
            System.out.println("Files for peer with ID = " + p.peerID + ":");
            for(MyFileObj f: p.files) {
                System.out.println("  File name = " + f.fileName);
                System.out.println("  File size = " + f.fileSize + " bytes\n");
                //System.out.println("  File data = " + f.fileData  + "\n");
            }
            System.out.println("***********************************************");
            System.out.println("***********************************************\n");
        }
    }

    /**
     * Registry is called by the peer to register all of its files with the
     * indexing server. The server builds the index for the peer.
     *
     * @param  int peerID [ID of the peer client.]
     * @param  MyFileObj[] files [Files to register with the index server.]
     * @return [boolean]
     */
    public boolean registry(int peerID, MyFileObj[] files) {
        // Thread synchronization for the registration of files from the peer clients
        synchronized (this) {
            try {
                // register files with index server
                Peer peer = new Peer(peerID, files);
                indexTable.add(peer);
                System.out.println("Peer with id = " + peerID +
                    " added to index table.\n");
                printContentsOfIndexTable();
                return true;
            }
            catch(Exception e) {
                System.err.println("ERROR: " + e);
                System.out.println("\nThere was a problem registering peer with id = " + peerID + ".\n");
                e.printStackTrace(System.out);
                return false;
            }
        }
    }

    /**
     * Search the index table and return all the matching peers with fileName
     * as the name of one of their files.
     *
     * @param String fileName [File to be searched when called by the peer client.]
     * @return [ Peer[] ]
     */
    public Peer[] search(String fileName) {
        List<Peer> matchingPeers = new ArrayList<Peer>();

        // Get list of files from each peer
        for(Peer p : indexTable) {
            // search list of files for a file that matches 'fileName'
            for(MyFileObj f: p.files) {
                if(f.fileName.equals(fileName)) {
                    matchingPeers.add(p);
                }
            }
        }

        return matchingPeers.toArray(new Peer[0]);
    }

    /**
     * Shutdown server from client.
     *
     * @return [void]
     */
    public void shutdown() {
        orb.shutdown(false);
    }
}

public class IndexServer
{
    static final int INDEX_SERVER_THREAD_POOL_SIZE = 5;

    // Create thread pool to handle multiple requests concurrently
    static ExecutorService serverThreadPool = Executors.newFixedThreadPool(INDEX_SERVER_THREAD_POOL_SIZE);

    /**
     * Index server main method.
     *
     * @param String args[]
     * @return [void]
     */
    public static void main(String args[]) {
        // Thread pool will handle multiple requests from peer clients
        serverThreadPool.submit(() -> {
            try {
                // create and initialize the ORB
                ORB orb = ORB.init(args, null);

                // get reference to rootpoa & activate the POAManager
                POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
                rootpoa.the_POAManager().activate();

                // create servant and register it with the ORB
                IndexServerFileSharingImpl serverImpl = new IndexServerFileSharingImpl();
                serverImpl.setORB(orb);

                // get object reference from the servant
                org.omg.CORBA.Object ref = rootpoa.servant_to_reference(serverImpl);
                IndexServerFileSharing href = IndexServerFileSharingHelper.narrow(ref);

                // get the root naming context
                // NameService invokes the name service
                org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
                // Use NamingContextExt which is part of the Interoperable
                // Naming Service (INS) specification.
                NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                // bind the Object Reference in Naming
                String name = "IndexServer";
                NameComponent path[] = ncRef.to_name(name);
                ncRef.rebind(path, href);

                System.out.println("Index server ready and waiting...\n");

                // wait for invocations from peer clients
                orb.run();
            }
            catch(Exception e) {
                System.err.println("ERROR: " + e);
                e.printStackTrace(System.out);
            }

            System.out.println("Index server exiting...\n");
        });
        // serverThreadPool.shutdown();
    }
}
