package NapDemoApp;


/**
* NapDemoApp/FilesToRegisterHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from NapDemo.idl
* Friday, March 31, 2017 8:57:03 AM CDT
*/

public final class FilesToRegisterHolder implements org.omg.CORBA.portable.Streamable
{
  public NapDemoApp.MyFileObj value[] = null;

  public FilesToRegisterHolder ()
  {
  }

  public FilesToRegisterHolder (NapDemoApp.MyFileObj[] initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = NapDemoApp.FilesToRegisterHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    NapDemoApp.FilesToRegisterHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return NapDemoApp.FilesToRegisterHelper.type ();
  }

}
