package NapDemoApp;


/**
* NapDemoApp/PeerServerFileSharingHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from NapDemo.idl
* Friday, March 31, 2017 8:57:03 AM CDT
*/

abstract public class PeerServerFileSharingHelper
{
  private static String  _id = "IDL:NapDemoApp/PeerServerFileSharing:1.0";

  public static void insert (org.omg.CORBA.Any a, NapDemoApp.PeerServerFileSharing that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static NapDemoApp.PeerServerFileSharing extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = org.omg.CORBA.ORB.init ().create_interface_tc (NapDemoApp.PeerServerFileSharingHelper.id (), "PeerServerFileSharing");
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static NapDemoApp.PeerServerFileSharing read (org.omg.CORBA.portable.InputStream istream)
  {
    return narrow (istream.read_Object (_PeerServerFileSharingStub.class));
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, NapDemoApp.PeerServerFileSharing value)
  {
    ostream.write_Object ((org.omg.CORBA.Object) value);
  }

  public static NapDemoApp.PeerServerFileSharing narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof NapDemoApp.PeerServerFileSharing)
      return (NapDemoApp.PeerServerFileSharing)obj;
    else if (!obj._is_a (id ()))
      throw new org.omg.CORBA.BAD_PARAM ();
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      NapDemoApp._PeerServerFileSharingStub stub = new NapDemoApp._PeerServerFileSharingStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

  public static NapDemoApp.PeerServerFileSharing unchecked_narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof NapDemoApp.PeerServerFileSharing)
      return (NapDemoApp.PeerServerFileSharing)obj;
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      NapDemoApp._PeerServerFileSharingStub stub = new NapDemoApp._PeerServerFileSharingStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

}
